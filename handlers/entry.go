package handlers

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"

	"code.sajari.com/docconv"
	"github.com/tealeg/xlsx"
	// "github.com/yanghuxiao/goxls2txt"
)

var outputText = ""

// CommonHeader sets header to all of handlers
func CommonHeader(w http.ResponseWriter) {
	w.Header().Set("Server", "Go-Event")
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "http://localhost:8080")
	w.Header().Set("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Accept-Language, Content-Language, Content-Type, x-xsrf-token, authorization")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	//w.WriteHeader(200)
}

// SendOptions to client
func SendOptions(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Server", "Go-Event")
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "http://localhost:8080")
	w.Header().Set("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Accept-Language, Content-Language, Content-Type, x-xsrf-token, authorization")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.WriteHeader(200)
}

func ParseDoc(w http.ResponseWriter, r *http.Request) {
	CommonHeader(w)
	// Parse our multipart form, 10 << 20 specifies a maximum
	// upload of 10 MB files.
	r.ParseMultipartForm(10 << 20)
	// FormFile returns the first file for the given key `myFile`
	// it also returns the FileHeader so we can get the Filename,
	// the Header and the size of the file
	file, handler, err := r.FormFile("myFile")
	if err != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
		return
	}
	defer file.Close()
	// fmt.Printf("Uploaded File: %+v\n", handler.Filename)
	// fmt.Printf("File Size: %+v\n", handler.Size)
	// fmt.Printf("MIME Header: %+v\n", handler.Header)

	randomFileName := rand.Intn(1000)

	samessage := strings.Split(handler.Filename, ".")
	ending := "." + samessage[len(samessage)-1]

	f, _ := os.OpenFile(strconv.Itoa(randomFileName)+ending, os.O_WRONLY|os.O_CREATE, 0666)
	defer f.Close()
	io.Copy(f, file)
	f.Close()

	start := time.Now()

	if ending == ".xls" {
		//parseXls(strconv.Itoa(randomFileName), ending)
		parseXlsx(strconv.Itoa(randomFileName), ending)
	} else if ending == ".xlsx" {
		parseXlsx(strconv.Itoa(randomFileName), ending)
	} else {
		parseCommonDocs(strconv.Itoa(randomFileName), ending)
	}

	elapsed := time.Since(start)

	out, err := exec.Command("python", "model.py", strconv.Itoa(randomFileName)+"temptext.txt").Output()
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Reading took %s", elapsed)

	//fmt.Println(res)

	//w.Write([]byte(text))
	if r.FormValue("text") == "true" {
		w.Write([]byte(outputText))
	} else {
		w.Write(out)
	}
	os.Remove(strconv.Itoa(randomFileName) + "temptext.txt")
	fmt.Println(strconv.Itoa(randomFileName) + ending)
	os.Remove(strconv.Itoa(randomFileName) + ending)
}

// func parseXls(tempfilename string, ending string) {
// 	fmt.Println("convert start")
// 	gx2t := goxls2txt.NewGoXls2Txt()
// 	_, err := gx2t.Xls2txt(tempfilename+ending, tempfilename+"temptext.txt")
// 	if err == nil {
// 		fmt.Println("convert xls to txt successfull")
// 	} else {
// 		fmt.Println(err)
// 	}
// }

func parseXlsx(tempfilename string, ending string) {
	xlFile, err := xlsx.OpenFile(tempfilename + ending)
	if err != nil {
		fmt.Println(err)
	}
	text := ""
	for _, sheet := range xlFile.Sheets {
		for _, row := range sheet.Rows {
			for _, cell := range row.Cells {
				text = text + " " + cell.String()
				//fmt.Printf("%s\n", text)
			}
		}
	}

	outputText = text
	err = ioutil.WriteFile(tempfilename+"temptext.txt", []byte(text), 0644)
	if err != nil {
		panic(err)
	}
}

func parsePpt(tempfilename string, ending string) {
}

func parseCommonDocs(tempfilename string, ending string) {
	res, _ := docconv.ConvertPath(tempfilename + ending)

	outputText = res.Body
	err := ioutil.WriteFile(tempfilename+"temptext.txt", []byte(res.Body), 0644)
	if err != nil {
		panic(err)
	}
}
