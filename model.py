#Path to files
path_model = 'modelRF.pickle'
path_vect    = 'tf_idf_vect.pickle'

#Load libs
from preprocess import preprocess
import numpy as np
import pickle
import sys

#Load preprocessing text -> preprocess text
parser = preprocess()

#Load vectorizer text -> float array
vect = pickle.load(open(path_vect, 'rb'))

#Load model floar_array -> float (probability)
model = pickle.load(open(path_model, 'rb'))

"""
Code before exicute long time ~3-5 sec
But it need implement only 1 time
Only one actions : memory allocation

"""
def predict_proba(text):
    preprocess_text = parser.parse(text)
    x = vect.transform([preprocess_text]).toarray()
    res = model.predict_proba(x)
    return res[0][1]

if __name__ == '__main__':
    text = ''
    with open(sys.argv[1], 'r', encoding="utf8") as file:
        text = file.read()
    #text = sys.argv[1]
    #print(text)
    res = predict_proba(text)
    print(res)