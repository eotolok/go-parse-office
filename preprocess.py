from pymystem3 import Mystem
from tqdm import tqdm_notebook
import sys, os
import pickle
import re
import time
import os

class preprocess():
    words = {}
    def get_stemmer(self):
        if (os.name == "posix"):
            mystem = Mystem(mystem_bin="./mystem")
            return mystem
        else:
            mystem = Mystem(mystem_bin="mystem.exe")
            return mystem
        
    
    def preprocess_text(self, text):
        text = text.lower()
        cleanr = re.compile("<.*?>")
        text = re.sub(cleanr, ' ', text)
        text = re.sub(r'[?|!|\'|"|#|1]', 'r', text)
        text = re.sub(r'[.|,|)|(|\|/]', r'', text)
        text = re.sub(r'(\nnan\n)|(\nnat\n)', '\n', text)
        return text
    
    def tokenize(self, text):
        not_empty_word = lambda x: (re.match('\s', x) is None )
        # print(not_empty_word('123jhksh '))
        tokens = re.split('\s', text)
        tokens = [i for i in tokens if not_empty_word(i)]
        return tokens 
    
    def lemmatize(self, tokens):
        temp_lemms = {}
        answ = self.mystem.lemmatize(' '.join(tokens))
        #for token in tqdm_notebook(tokens):
        #    if token in self.words:
        #        pass
        #    else:
        #        self.words[token] = self.mystem.lemmatize(token)
        #    answ.append(self.words[token])
        return answ#' '.join(answ)
            
    def __init__(self):
        self.mystem = self.get_stemmer()
        
    def parse(self, text):
        text   = self.preprocess_text(text)
        tokens = self.tokenize(text)
        #self.tokens = tokens
        lemmas = self.lemmatize(tokens)
        return ' '.join(lemmas)
    
def preprocess_data(path_parsed_data, path_normalized_data, _ignore_already_done_files = False):
    files_parsed = [os.path.join(path_parsed_data, i) for i in os.listdir(path_parsed_data)]
    files_index  = [os.path.join(path_normalized_data, i) for i in os.listdir(path_normalized_data)]
    parser = preprocess()
    log_time = {}
    for c, fn in tqdm_notebook(enumerate(files_parsed), total = len(files_parsed)):
        sys.stdout.write('\r %d/%d (%2.1f%%) %s %s'%(c, len(files_parsed), c*100.0/len(files_parsed), fn, " "*60))
        obj = pickle.load(open(fn, 'rb'))
        ID = fn.split('/')[-1].split('.')[0]                               
        fn_out = os.path.join(path_normalized_data, ID)+'.txt'
        #print(fn_out)
        #print(files_index[:2])
        if not _ignore_already_done_files:
            if fn_out in files_index:
                #print('\n\n!!!\n\n')
                continue
        time_var = time.time()
        res = parser.parse(obj.text)
        open(fn_out, 'w+', encoding = 'utf-8').write(res)
        time_var = time_var-time.time()
        log_time[ID] = time_var
        pickle.dump(log_time, open('parser_log_time.pickle', 'wb'))
    print('Done normalizing files')
    return
