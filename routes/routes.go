package routes

import (
	"net/http"

	"../handlers"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

//Up - startings all new routers
func Up() {
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	// Start api routes
	r.Route("/api", func(r chi.Router) {
		r.Post("/doc", handlers.ParseDoc)
		// r.Post("/xls", handlers.ParseXls)
		// r.Post("/ppt", handlers.ParsePpt)
	})
	http.ListenAndServe(":14880", r)
}
